package com.charles.bluetoothReceiver;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends Activity implements OnClickListener, OnItemSelectedListener {
  // Standard uuid
  private static UUID SERIAL_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

  // Thread states
  private static int BACKGROUNDED_CLIENT = 0;
  private static int BACKGROUNDED_SERVER = 1;
  private static int PAUSED = 2;
  private static int RUNNING_CLIENT = 3;
  private static int RUNNING_SERVER = 4;
  private static int CLOSING = 5;

  // Client Thread fields
  private ClientBluetoothThread clientThread;
  private BluetoothSocket clientSocket;

  // Server Thread fields
  private ServerBluetoothThread serverThread;
  private BluetoothServerSocket listenSocket;
  private BluetoothSocket serverSocket;

  // Bluetooth Receiver fields
  private BluetoothReceiver receiver;
  
  // Bluetooth Device
  private DeviceAdapter adapter;

  // State fields
  private BluetoothDevice device;
  private int state;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    try {
      super.onCreate(savedInstanceState);
      this.setContentView(R.layout.activity_main);

      // Initialize and start the threads
      clientThread = new ClientBluetoothThread();
      clientThread.setName("Client Bluetooth Thread");
      clientThread.start();
      serverThread = new ServerBluetoothThread();
      serverThread.setName("Server Bluetooth Thread");
      serverThread.start();

      // Wire up the events
      ((Button) findViewById(R.id.device_add_button)).setOnClickListener(this);
      ((Button) findViewById(R.id.device_server_connect_button)).setOnClickListener(this);
      ((Button) findViewById(R.id.device_client_connect_button)).setOnClickListener(this);
      ((Button) findViewById(R.id.device_stop_button)).setOnClickListener(this);
      ((Button) findViewById(R.id.app_close_button)).setOnClickListener(this);
      ((Spinner) findViewById(R.id.device_list_view)).setOnItemSelectedListener(this);

      // Wire up the receiver
      receiver = new BluetoothReceiver();
      IntentFilter intent = new IntentFilter();
      intent.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
      intent.addAction(BluetoothDevice.ACTION_FOUND);
      intent.addAction(BluetoothDevice.ACTION_NAME_CHANGED);
      intent.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
      registerReceiver(receiver, intent);
      
      adapter = new DeviceAdapter(this);

      if (BluetoothAdapter.getDefaultAdapter().isEnabled()) {
        // Initialize the device list
        for (BluetoothDevice boundDevice : BluetoothAdapter.getDefaultAdapter().getBondedDevices()) {
          adapter.add(boundDevice);
        }
        ((Spinner) findViewById(R.id.device_list_view)).setAdapter(adapter);
        adapter.notifyDataSetChanged();

        // Adjust the state
        state = MainActivity.PAUSED;
        ((Button) findViewById(R.id.device_server_connect_button)).setEnabled(true);
        ((Button) findViewById(R.id.device_client_connect_button)).setEnabled(true);
        ((Button) findViewById(R.id.device_stop_button)).setEnabled(false);
      }
      else {
        // Initialize with no the data
        adapter.clear();
        adapter.notifyDataSetChanged();

        // Adjust the state
        state = MainActivity.PAUSED;
        ((Button) findViewById(R.id.device_server_connect_button)).setEnabled(false);
        ((Button) findViewById(R.id.device_client_connect_button)).setEnabled(false);
        ((Button) findViewById(R.id.device_stop_button)).setEnabled(false);
      }
    }
    catch (Exception e) {
      Log.e("BluetoothReceiver", "Unknown exception on create.", e);
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
    if (state == MainActivity.BACKGROUNDED_SERVER) {
      state = MainActivity.RUNNING_SERVER;
    }
    else if (state == MainActivity.BACKGROUNDED_CLIENT) {
      state = MainActivity.RUNNING_CLIENT;
    }
  }

  @Override
  protected void onPause() {
    super.onPause();
    if (state == MainActivity.RUNNING_SERVER) {
      serverThread.cleanClose();
      state = MainActivity.BACKGROUNDED_SERVER;
    }
    else if (state == MainActivity.RUNNING_CLIENT) {
      clientThread.cleanClose();
      state = MainActivity.BACKGROUNDED_CLIENT;
    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    unregisterReceiver(receiver);
    serverThread.cleanClose();
    clientThread.cleanClose();
    state = MainActivity.CLOSING;
  }

  @Override
  public void onClick(View v) {
    if (v.getId() == R.id.device_add_button) {
      Intent intent = new Intent(this, DeviceAddActivity.class);
      this.startActivityForResult(intent, DeviceAddActivity.ALERT_CODE);
    }
    else if (v.getId() == R.id.device_server_connect_button) {
      if (state == MainActivity.RUNNING_SERVER) {
        serverThread.cleanClose();
      }
      else if ((state == MainActivity.RUNNING_CLIENT)) {
        clientThread.cleanClose();
      }
      if (BluetoothAdapter.getDefaultAdapter().isEnabled()) {
        clearScreen();
        state = MainActivity.RUNNING_SERVER;
        ((Button) findViewById(R.id.device_server_connect_button)).setEnabled(false);
        ((Button) findViewById(R.id.device_client_connect_button)).setEnabled(true);
        ((Button) findViewById(R.id.device_stop_button)).setEnabled(true);
      }
    }
    else if (v.getId() == R.id.device_client_connect_button) {
      if (state == MainActivity.RUNNING_SERVER) {
        serverThread.cleanClose();
      }
      else if ((state == MainActivity.RUNNING_CLIENT)) {
        clientThread.cleanClose();
      }
      if (BluetoothAdapter.getDefaultAdapter().isEnabled()) {
        clearScreen();
        state = MainActivity.RUNNING_CLIENT;
        ((Button) findViewById(R.id.device_server_connect_button)).setEnabled(true);
        ((Button) findViewById(R.id.device_client_connect_button)).setEnabled(false);
        ((Button) findViewById(R.id.device_stop_button)).setEnabled(true);
      }
    }
    else if (v.getId() == R.id.device_stop_button) {
      if (state == MainActivity.RUNNING_SERVER) {
        serverThread.cleanClose();
      }
      else if ((state == MainActivity.RUNNING_CLIENT)) {
        clientThread.cleanClose();
      }
      state = MainActivity.PAUSED;
      ((Button) findViewById(R.id.device_server_connect_button)).setEnabled(true);
      ((Button) findViewById(R.id.device_client_connect_button)).setEnabled(true);
      ((Button) findViewById(R.id.device_stop_button)).setEnabled(false);
    }
    else if (v.getId() == R.id.app_close_button) {
      finish();
    }
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (resultCode == Activity.RESULT_OK) {
      if (requestCode == DeviceAddActivity.ALERT_CODE) {
        Bundle bundle = data.getExtras();
        String newMacAddress = null;
        BluetoothDevice newDevice = null;
        if (bundle != null) {
          newMacAddress = bundle.getString("macAddress");
        }
        if (newMacAddress != null) {
          newDevice = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(newMacAddress);
        }
        if (newDevice != null) {
          if ((state == MainActivity.RUNNING_SERVER) || (state == MainActivity.RUNNING_CLIENT)) {
            state = MainActivity.PAUSED;
            ((Button) findViewById(R.id.device_server_connect_button)).setEnabled(true);
            ((Button) findViewById(R.id.device_client_connect_button)).setEnabled(true);
            ((Button) findViewById(R.id.device_stop_button)).setEnabled(false);
          }

          device = newDevice;
        }
      }
    }
  }

  @Override
  public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
    if ((state == MainActivity.RUNNING_SERVER) || (state == MainActivity.RUNNING_CLIENT)) {
      state = MainActivity.PAUSED;
      ((Button) findViewById(R.id.device_server_connect_button)).setEnabled(true);
      ((Button) findViewById(R.id.device_client_connect_button)).setEnabled(true);
      ((Button) findViewById(R.id.device_stop_button)).setEnabled(false);
    }
    device = (BluetoothDevice) adapter.getItem(position);
  }

  @Override
  public void onNothingSelected(AdapterView<?> adapterView) {
    ((Button) findViewById(R.id.device_server_connect_button)).setEnabled(false);
    ((Button) findViewById(R.id.device_client_connect_button)).setEnabled(false);
    ((Button) findViewById(R.id.device_stop_button)).setEnabled(false);
    device = null;
  }

  private void appendToScreen(final String text) {
    if (text.length() > 0) {
      runOnUiThread(new Runnable() {
        @Override
        public void run() {
          String preText = ((TextView) findViewById(R.id.device_output_text_view)).getText().toString();
          ((TextView) findViewById(R.id.device_output_text_view)).setText(preText + text);
        }
      });
    }
  }

  private void clearScreen() {
    runOnUiThread(new Runnable() {
      @Override
      public void run() {
        ((TextView) findViewById(R.id.device_output_text_view)).setText("");
      }
    });
  }

  private class ClientBluetoothThread extends Thread {
    @Override
    public void run() {
      // Loop until closing
      while (state != MainActivity.CLOSING) {
        try {
          // Block until running
          while (state != MainActivity.RUNNING_CLIENT) {
            if (state == MainActivity.CLOSING) {
              return;
            }
          }
          if (device != null) {
            InputStream input = null;

            // Connect to the device
            try {
              clientSocket = device.createRfcommSocketToServiceRecord(MainActivity.SERIAL_UUID);
              BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
              clientSocket.connect();
              input = clientSocket.getInputStream();
            }
            catch (IOException e) {
              Log.e("BluetoothReceiver", "Could not connect to device.");
              continue;
            }

            // Read input as long as possible
            while (state == MainActivity.RUNNING_CLIENT) {
              try {
                if (!clientSocket.isConnected()) {
                  Log.e("BluetoothReceiver", "Lost connection to device.");
                  break;
                }
                if (input.available() > 0) {
                  int read = input.read();
                  Log.i("BluetoothReceiver", "Read byte " + read + " '" + (char) read + "'");
                  if (read == 2) {
                    appendToScreen("<STX>");
                  }
                  else if (read == 3) {
                    appendToScreen("<ETX>");
                  }
                  else if (read == 28) {
                    appendToScreen("<FS>");
                  }
                  else if (read == 13) {
                    appendToScreen("<CR>");
                  }
                  else {
                    appendToScreen(String.valueOf((char) read));
                  }
                }
              }
              catch (IOException e) {
                Log.e("BluetoothReceiver", "Error reading from device.");
                break;
              }
            }

            // Output the data on stop

            cleanClose();
          }
        }
        catch (Exception e) {
          Log.e("BluetoothReceiver", "Unknown exception in read thread.", e);
        }
        finally {
          cleanClose();
        }
      }
    }

    private void cleanClose() {
      // Cleanly close
      try {
        if (clientSocket != null) {
          clientSocket.close();
        }
      }
      catch (IOException e) {
        Log.w("BluetoothReceiver", "Socket could not cleanly close.");
      }
    }
  }

  private class ServerBluetoothThread extends Thread {
    @Override
    public void run() {
      // Loop until closing
      while (state != MainActivity.CLOSING) {
        try {
          // Block until running
          while (state != MainActivity.RUNNING_SERVER) {
            if (state == MainActivity.CLOSING) {
              return;
            }
          }
          if (device != null) {
            InputStream input = null;

            try {
              listenSocket = BluetoothAdapter.getDefaultAdapter().listenUsingRfcommWithServiceRecord("PWAccessP", MainActivity.SERIAL_UUID);
            }
            catch (IOException e) {
              Log.e("BluetoothReceiver", "Could not listen for device.");
              continue;
            }

            if (state != MainActivity.RUNNING_SERVER) {
              continue;
            }

            try {
              serverSocket = listenSocket.accept();
              if (device.getAddress().equals(serverSocket.getRemoteDevice().getAddress())) {
                input = serverSocket.getInputStream();
              }
              else {
                Log.w("BluetoothReceiver", "Connected to wrong device.");
                continue;
              }
            }
            catch (IOException e) {
              Log.e("BluetoothReceiver", "Could not connect to device.");
              continue;
            }

            // Read input as long as possible
            while (state == MainActivity.RUNNING_SERVER) {
              try {
                if (!serverSocket.isConnected()) {
                  Log.e("BluetoothReceiver", "Lost connection to device.");
                  break;
                }
                if (input.available() > 0) {
                  int read = input.read();
                  Log.i("BluetoothReceiver", "Read byte " + read + " '" + (char) read + "'");
                  if (read == 2) {
                    appendToScreen("<STX>");
                  }
                  else if (read == 3) {
                    appendToScreen("<ETX>");
                  }
                  else if (read == 28) {
                    appendToScreen("<FS>");
                  }
                  else if (read == 13) {
                    appendToScreen("<CR>");
                  }
                  else {
                    appendToScreen(String.valueOf((char) read));
                  }
                }
              }
              catch (IOException e) {
                Log.e("BluetoothReceiver", "Error reading from device.");
                break;
              }
            }

            // Cleanly close
            cleanClose();
          }
        }
        finally {
          cleanClose();
        }
      }
    }

    private void cleanClose() {
      // Cleanly close
      try {
        if (listenSocket != null) {
          listenSocket.close();
        }
      }
      catch (IOException e) {
        Log.w("BluetoothReceiver", "Socket could not cleanly close.");
      }
      try {
        if (serverSocket != null) {
          serverSocket.close();
        }
      }
      catch (IOException e) {
        Log.w("BluetoothReceiver", "Socket could not cleanly close.");
      }
    }
  }

  private class BluetoothReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
      String action = intent.getAction();
      if ((BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action))
          || (BluetoothDevice.ACTION_FOUND.equals(action))
          || (BluetoothDevice.ACTION_NAME_CHANGED.equals(action))) {
        BluetoothDevice newDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
        adapter.add(newDevice);
        adapter.notifyDataSetChanged();
      }
      else if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
        int bluetoothState = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1);
        if (BluetoothAdapter.STATE_ON == bluetoothState) {
          // Re-initialize the device list
          adapter.clear();
          for (BluetoothDevice boundDevice : BluetoothAdapter.getDefaultAdapter().getBondedDevices()) {
            adapter.add(boundDevice);
          }
          adapter.notifyDataSetChanged();

          // Adjust the state
          bluetoothState = MainActivity.PAUSED;
          ((Button) findViewById(R.id.device_server_connect_button)).setEnabled(true);
          ((Button) findViewById(R.id.device_client_connect_button)).setEnabled(true);
          ((Button) findViewById(R.id.device_stop_button)).setEnabled(false);
        }
        else {
          // Remove the data
          adapter.clear();
          adapter.notifyDataSetChanged();

          // Adjust the state
          bluetoothState = MainActivity.PAUSED;
          ((Button) findViewById(R.id.device_server_connect_button)).setEnabled(false);
          ((Button) findViewById(R.id.device_client_connect_button)).setEnabled(false);
          ((Button) findViewById(R.id.device_stop_button)).setEnabled(false);
        }
      }
    }
  }
}