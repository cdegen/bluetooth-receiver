package com.charles.bluetoothReceiver;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class DeviceAddActivity extends Activity implements OnClickListener {
  public static final int ALERT_CODE = 7838;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    try {
      super.onCreate(savedInstanceState);
      this.setContentView(R.layout.device_add);

      ((Button) findViewById(R.id.accept_button)).setOnClickListener(this);
      ((Button) findViewById(R.id.reject_button)).setOnClickListener(this);
    }
    catch (Exception e) {
      Log.e("BluetoothReceiver", "Unknown exception on create.", e);
    }
  }

  @Override
  public void onClick(View v) {
    if (v.getId() == R.id.accept_button) {
      Intent data = new Intent();
      data.putExtra("macAddress", ((EditText) findViewById(R.id.mac_address_text_edit)).getText().toString());
      this.setResult(Activity.RESULT_OK, data);
      finish();
    }
    else if (v.getId() == R.id.reject_button) {
      this.setResult(Activity.RESULT_CANCELED);
      finish();
    }
  }
}
