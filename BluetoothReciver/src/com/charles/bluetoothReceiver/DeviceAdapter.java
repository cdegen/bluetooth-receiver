package com.charles.bluetoothReceiver;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class DeviceAdapter extends ArrayAdapter<BluetoothDevice> {
  public DeviceAdapter(Context context) {
    super(context, R.layout.device);
  }

  @Override
  public boolean hasStableIds() {
    return false;
  }
  

  @Override
  public View getDropDownView(int position, View convertView, ViewGroup parent) {
    LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View rowView = inflater.inflate(R.layout.device, parent, false);

    BluetoothDevice item = getItem(position);
    if (item != null) {
      ((TextView) rowView.findViewById(R.id.device_name_text)).setText(item.getName());
      ((TextView) rowView.findViewById(R.id.device_mac_address_text)).setText(item.getAddress());
      switch (item.getBondState()) {
        case BluetoothDevice.BOND_BONDED:
          ((TextView) rowView.findViewById(R.id.device_bond_state_text)).setText("Paired");
          break;
        case BluetoothDevice.BOND_BONDING:
          ((TextView) rowView.findViewById(R.id.device_bond_state_text)).setText("Pairing...");
          break;
        case BluetoothDevice.BOND_NONE:
          ((TextView) rowView.findViewById(R.id.device_bond_state_text)).setText("Not Paired");
          break;
      }
    }

    return rowView;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View rowView = inflater.inflate(R.layout.device, parent, false);

    BluetoothDevice item = getItem(position);
    if (item != null) {
      ((TextView) rowView.findViewById(R.id.device_name_text)).setText(item.getName());
      ((TextView) rowView.findViewById(R.id.device_mac_address_text)).setText(item.getAddress());
      switch (item.getBondState()) {
        case BluetoothDevice.BOND_BONDED:
          ((TextView) rowView.findViewById(R.id.device_bond_state_text)).setText("Paired");
          break;
        case BluetoothDevice.BOND_BONDING:
          ((TextView) rowView.findViewById(R.id.device_bond_state_text)).setText("Pairing...");
          break;
        case BluetoothDevice.BOND_NONE:
          ((TextView) rowView.findViewById(R.id.device_bond_state_text)).setText("Not Paired");
          break;
      }
    }

    return rowView;
  }
}
